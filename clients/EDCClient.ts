///<reference path="../node_modules/@types/node/index.d.ts"/>

import * as http from 'http';

interface EDCErrorDetails {
    message: string;
    stackTrace: string;
}

interface EDCRestDomainGroup {
    groupName: string;
    description?: string;
    createdBy?: string;
    groupId?: string;
    dataDomains?: EDCRestDataDomain[];
}

interface EDCRestDataDomain {
    createdBy?: string;
    description?: string;
    domainGrps?: EDCRestDomainGroup[];
    domainId?: string;
    domainName?: string;
}


interface EDCBulkEnrichObjectRequest {
    providerId: string;
    updates: EDCEnrichObjectRequest[];
}

interface EDCEnrichObjectRequest {
    id: string;
    newFacts: EDCEnrichFactRequest[];
    newSourceLinks?: EDCEnrichLinkRequest[];
}

interface EDCEnrichFactRequest {
    attrUuid: string;
    value: string;
}

interface EDCEnrichLinkRequest {
    associationId: string;
    objectId: string;
    properties: EDCEnrichFactRequest[];
}

interface EDCPublishResponse {
    metadata: EDCPublishResponseMetadata;
}

interface EDCPublishResponseMetadata {
    processingTime: number,
    publishTimestamp?: number;
}

export interface EDCClientConfiguration {
    hostname: string;
    port: number;
    username: string;
    password: string;

    resource_name: string;
    resource_path?: string;
}

export class EDCClient {

    private static getAuthorizationHeader(configuration: EDCClientConfiguration): string {
        return 'Basic ' + Buffer.from(configuration.username + ':' + configuration.password).toString('base64')
    }

    public static createDataDomainGroup(configuration: EDCClientConfiguration, name: string, description: string = '', return_value_if_already_exists: boolean = true): Promise<boolean> {

        const request_body_object: EDCRestDomainGroup = {
            groupName: name,
            description: description,
            createdBy: configuration.username
        };
        const request_body_string = JSON.stringify(request_body_object);

        const request_options = {
            method: 'POST',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domaingrps',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(request_body_string)
            }
        };

        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);

        console.info('Creating Data Domain Group [' + name + ']. Options [' + JSON.stringify(request_options) + '].');

        return new Promise((resolve, reject) => {

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    if (response_body_string && response_body_string.length) {
                        try {
                            const response_body_object: EDCErrorDetails = JSON.parse(response_body_string);

                            if (response_body_object.message.indexOf('Specify a unique object name') >= 0) {
                                console.info('Data Domain Group [' + name + '] already exists.');
                                resolve(return_value_if_already_exists);
                            } else {
                                // Unknown error
                                const error_message = 'Error creating Data Domain Group [' + name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                        } catch (e) {
                            // Unknown error response (not JSON)
                            const error_message = 'Error creating Data Domain Group [' + name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    } else {
                        console.info('Completed creating Data Domain Group [' + name + '].');
                        resolve(true);
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error creating Data Domain Group [' + name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.write(request_body_string);
            request.end();
        });
    }

    public static getDataDomainGroupId(configuration: EDCClientConfiguration, name: string): Promise<string> {

        const request_options = {
            method: 'GET',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domaingrps/' + name,
            headers: {
                'Content-Type': 'application/json'
            }
        };

        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);

        console.info('Retrieving ID for Data Domain Group [' + name + ']. Options [' + JSON.stringify(request_options) + '].');

        return new Promise((resolve, reject) => {

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    if (response_body_string && response_body_string.length) {
                        try {
                            const response_body_object: EDCRestDomainGroup & EDCErrorDetails = JSON.parse(response_body_string);

                            if (response_body_object.message && response_body_object.message.indexOf('not found') >= 0) {
                                // DataDomainGroup does not exist
                                const error_message = 'Error retrieving ID for Data Domain Group [' + name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            } else {
                                const id = response_body_object.groupId;
                                console.info('Completed retrieving ID for Data Domain Group [' + name + ']. ID [' + id + ']');
                                resolve(id);
                            }
                        } catch (e) {
                            // Unknown error response (not JSON)
                            const error_message = 'Error retrieving ID for Data Domain Group [' + name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    } else {
                        // Unknown error (no response)
                        const error_message = 'Error retrieving ID for Data Domain Group [' + name + ']. Response [' + response_body_string + '].';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error retrieving ID for Data Domain Group [' + name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.end();
        });
    }

    public static deleteDataDomainGroup(configuration: EDCClientConfiguration, name: string, return_value_if_does_not_exist: boolean = true): Promise<boolean> {

        const request_options = {
            method: 'DELETE',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domaingrps/' + name,
            headers: {
                'Content-Type': 'application/json'
            }
        };

        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);

        console.info('Deleting Data Domain Group [' + name + ']. Options [' + JSON.stringify(request_options) + '].');

        return new Promise((resolve, reject) => {

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    if (response_body_string && response_body_string.length) {
                        try {
                            const response_body_object: EDCErrorDetails = JSON.parse(response_body_string);

                            if (response_body_object.message.indexOf('not found') >= 0) {
                                console.info('Data Domain Group [' + name + '] does not exist.');
                                resolve(return_value_if_does_not_exist);
                            } else {
                                // Unknown error
                                const error_message = 'Error deleting Data Domain Group [' + name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                        } catch (e) {
                            // Unknown error response (not JSON)
                            const error_message = 'Error deleting Data Domain Group [' + name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    } else {
                        console.info('Completed deleting Data Domain Group [' + name + '].');
                        resolve(true);
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error deleting Data Domain Group [' + name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.end();
        });
    }

    public static createDataDomainWithinDataDomainGroup(configuration: EDCClientConfiguration, data_domain_group_id: string, data_domain_group_name: string, data_domain_name: string, description = '', return_value_if_already_exists: boolean = true): Promise<boolean> {

        const request_body_object: EDCRestDataDomain = {
            domainGrps: [{groupName: data_domain_group_name, groupId: data_domain_group_id}],
            domainName: data_domain_name,
            description: description,
            createdBy: configuration.username
        };
        const request_body_string = JSON.stringify(request_body_object);

        const request_options = {
            method: 'POST',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domains',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(request_body_string)
            }
        };

        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);

        console.info('Creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. Options [' + JSON.stringify(request_options) + '].');

        return new Promise((resolve, reject) => {

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    if (response_body_string && response_body_string.length) {
                        try {
                            const response_body_object: EDCErrorDetails = JSON.parse(response_body_string);

                            if (response_body_object.message.indexOf('Specify a unique object name') >= 0) {
                                // Data Domain already exists
                                console.info('Data Domain [' + data_domain_name + '] already exists.');
                                resolve(return_value_if_already_exists);
                            } else {
                                // Unknown error
                                const error_message = 'Error creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                        } catch (e) {
                            // Unknown error response (not JSON)
                            const error_message = 'Error creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    } else {
                        console.info('Completed creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. New DataDomain created.');
                        resolve(true);
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.write(request_body_string);
            request.end();
        });
    }

    public static deleteDataDomain(configuration: EDCClientConfiguration, name: string, return_value_if_does_not_exist: boolean = true): Promise<boolean> {

        const request_options = {
            method: 'DELETE',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domains/' + name,
            headers: {
                'Content-Type': 'application/json'
            }
        };

        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);

        console.info('Deleting Data Domain [' + name + ']. Options [' + JSON.stringify(request_options) + '].');

        return new Promise((resolve, reject) => {

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    if (response_body_string && response_body_string.length) {
                        try {
                            const response_body_object: EDCErrorDetails = JSON.parse(response_body_string);

                            if (response_body_object.message.indexOf('not found') >= 0) {
                                console.info('Data Domain [' + name + '] does not exist.');
                                resolve(return_value_if_does_not_exist);
                            } else {
                                // Unknown error
                                const error_message = 'Error deleting Data Domain [' + name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                        } catch (e) {
                            // Unknown error response (not JSON)
                            const error_message = 'Error deleting Data Domain [' + name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    } else {
                        console.info('Completed deleting Data Domain [' + name + '].');
                        resolve(true);
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error deleting Data Domain [' + name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.end();
        });
    }

    public static appendDataDomainsToDataObject(configuration: EDCClientConfiguration, data_object_name: string, data_domain_names: string[]): Promise<boolean> {

        const document_id = configuration.resource_name + '://' + configuration.resource_path + '/' + data_object_name;

        // Add Terms
        const newFacts: EDCEnrichFactRequest[] = [];
        const newSourceLinks: EDCEnrichLinkRequest[] = [];

        for (let i = 0; i < data_domain_names.length; i++) {
            const data_domain_name = data_domain_names[i];
            const newFact1: EDCEnrichFactRequest = {
                attrUuid: 'com.infa.ldm.profiling.DataDomainDataSetAll',
                value: 'DataDomain://' + data_domain_name
            };
            newFacts.push(newFact1);

            const newFact2: EDCEnrichFactRequest = {
                attrUuid: 'com.infa.ldm.profiling.DataDomainDataSetAccepted',
                value: 'DataDomain://' + data_domain_name
            };
            newFacts.push(newFact2);

            const newSourceLink1: EDCEnrichLinkRequest = {
                associationId: 'com.infa.ldm.profiling.DataDomainDataSetAll',
                objectId: 'DataDomain://' + data_domain_name,
                properties: [
                    {
                        attrUuid: 'core.targetAttribute',
                        value: 'com.infa.ldm.profiling.DataDomainDataSetAll'
                    }]
            };
            newSourceLinks.push(newSourceLink1);

            const newSourceLink2: EDCEnrichLinkRequest = {
                associationId: 'com.infa.ldm.profiling.DataDomainTable',
                objectId: 'DataDomain://' + data_domain_name,
                properties: [
                    {
                        attrUuid: 'core.name',
                        value: document_id
                    }]
            };
            newSourceLinks.push(newSourceLink2);
        }

        const request_body_object: EDCBulkEnrichObjectRequest = {
            providerId: 'DataDomainScanner',
            updates: [
                {
                    id: document_id,
                    newFacts: newFacts,
                    newSourceLinks: newSourceLinks
                }
            ]
        };
        const request_body_string = JSON.stringify(request_body_object);
        console.log(request_body_string);

        const request_options = {
            method: 'PATCH',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/data/objects',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(request_body_string)
            }
        };

        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);

        console.info('Updating Data Object [' + data_object_name + '] with Data Domains. length [' + data_domain_names.length + ']. Options [' + JSON.stringify(request_options) + '].');

        return new Promise((resolve, reject) => {

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    console.log(response_body_string);
                    try {
                        const response_body_object: EDCPublishResponse & EDCErrorDetails = JSON.parse(response_body_string);

                        if (response_body_object.message) {
                            // Unknown error response (is JSON)
                            const error_message = 'Error updating Data Object [' + data_object_name + '] with Data Domains. Response [' + response_body_object.message + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else {
                            resolve(true);
                        }
                    } catch (e) {
                        // Unknown error response (not JSON)
                        const error_message = 'Error updating Data Object [' + data_object_name + '] with Data Domains. Response [' + response_body_string + '].';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error updating Document (Object) [' + data_object_name + '] with Data Domains. length [' + data_domain_names.length + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.write(request_body_string);
            request.end();
        });
    }
}
