"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var EDCClient = (function () {
    function EDCClient() {
    }
    EDCClient.getAuthorizationHeader = function (configuration) {
        return 'Basic ' + Buffer.from(configuration.username + ':' + configuration.password).toString('base64');
    };
    EDCClient.createDataDomainGroup = function (configuration, name, description, return_value_if_already_exists) {
        if (description === void 0) { description = ''; }
        if (return_value_if_already_exists === void 0) { return_value_if_already_exists = true; }
        var request_body_object = {
            groupName: name,
            description: description,
            createdBy: configuration.username
        };
        var request_body_string = JSON.stringify(request_body_object);
        var request_options = {
            method: 'POST',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domaingrps',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(request_body_string)
            }
        };
        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);
        console.info('Creating Data Domain Group [' + name + ']. Options [' + JSON.stringify(request_options) + '].');
        return new Promise(function (resolve, reject) {
            var request = http.request(request_options, function (response) {
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    if (response_body_string && response_body_string.length) {
                        try {
                            var response_body_object = JSON.parse(response_body_string);
                            if (response_body_object.message.indexOf('Specify a unique object name') >= 0) {
                                console.info('Data Domain Group [' + name + '] already exists.');
                                resolve(return_value_if_already_exists);
                            }
                            else {
                                var error_message = 'Error creating Data Domain Group [' + name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                        }
                        catch (e) {
                            var error_message = 'Error creating Data Domain Group [' + name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    }
                    else {
                        console.info('Completed creating Data Domain Group [' + name + '].');
                        resolve(true);
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error creating Data Domain Group [' + name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.write(request_body_string);
            request.end();
        });
    };
    EDCClient.getDataDomainGroupId = function (configuration, name) {
        var request_options = {
            method: 'GET',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domaingrps/' + name,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);
        console.info('Retrieving ID for Data Domain Group [' + name + ']. Options [' + JSON.stringify(request_options) + '].');
        return new Promise(function (resolve, reject) {
            var request = http.request(request_options, function (response) {
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    if (response_body_string && response_body_string.length) {
                        try {
                            var response_body_object = JSON.parse(response_body_string);
                            if (response_body_object.message && response_body_object.message.indexOf('not found') >= 0) {
                                var error_message = 'Error retrieving ID for Data Domain Group [' + name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                            else {
                                var id = response_body_object.groupId;
                                console.info('Completed retrieving ID for Data Domain Group [' + name + ']. ID [' + id + ']');
                                resolve(id);
                            }
                        }
                        catch (e) {
                            var error_message = 'Error retrieving ID for Data Domain Group [' + name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    }
                    else {
                        var error_message = 'Error retrieving ID for Data Domain Group [' + name + ']. Response [' + response_body_string + '].';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error retrieving ID for Data Domain Group [' + name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.end();
        });
    };
    EDCClient.deleteDataDomainGroup = function (configuration, name, return_value_if_does_not_exist) {
        if (return_value_if_does_not_exist === void 0) { return_value_if_does_not_exist = true; }
        var request_options = {
            method: 'DELETE',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domaingrps/' + name,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);
        console.info('Deleting Data Domain Group [' + name + ']. Options [' + JSON.stringify(request_options) + '].');
        return new Promise(function (resolve, reject) {
            var request = http.request(request_options, function (response) {
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    if (response_body_string && response_body_string.length) {
                        try {
                            var response_body_object = JSON.parse(response_body_string);
                            if (response_body_object.message.indexOf('not found') >= 0) {
                                console.info('Data Domain Group [' + name + '] does not exist.');
                                resolve(return_value_if_does_not_exist);
                            }
                            else {
                                var error_message = 'Error deleting Data Domain Group [' + name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                        }
                        catch (e) {
                            var error_message = 'Error deleting Data Domain Group [' + name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    }
                    else {
                        console.info('Completed deleting Data Domain Group [' + name + '].');
                        resolve(true);
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error deleting Data Domain Group [' + name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.end();
        });
    };
    EDCClient.createDataDomainWithinDataDomainGroup = function (configuration, data_domain_group_id, data_domain_group_name, data_domain_name, description, return_value_if_already_exists) {
        if (description === void 0) { description = ''; }
        if (return_value_if_already_exists === void 0) { return_value_if_already_exists = true; }
        var request_body_object = {
            domainGrps: [{ groupName: data_domain_group_name, groupId: data_domain_group_id }],
            domainName: data_domain_name,
            description: description,
            createdBy: configuration.username
        };
        var request_body_string = JSON.stringify(request_body_object);
        var request_options = {
            method: 'POST',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domains',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(request_body_string)
            }
        };
        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);
        console.info('Creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. Options [' + JSON.stringify(request_options) + '].');
        return new Promise(function (resolve, reject) {
            var request = http.request(request_options, function (response) {
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    if (response_body_string && response_body_string.length) {
                        try {
                            var response_body_object = JSON.parse(response_body_string);
                            if (response_body_object.message.indexOf('Specify a unique object name') >= 0) {
                                console.info('Data Domain [' + data_domain_name + '] already exists.');
                                resolve(return_value_if_already_exists);
                            }
                            else {
                                var error_message = 'Error creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                        }
                        catch (e) {
                            var error_message = 'Error creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    }
                    else {
                        console.info('Completed creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. New DataDomain created.');
                        resolve(true);
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error creating Data Domain [' + data_domain_name + '] within Data Domain Group [' + data_domain_group_name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.write(request_body_string);
            request.end();
        });
    };
    EDCClient.deleteDataDomain = function (configuration, name, return_value_if_does_not_exist) {
        if (return_value_if_does_not_exist === void 0) { return_value_if_does_not_exist = true; }
        var request_options = {
            method: 'DELETE',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/datadomains/domains/' + name,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);
        console.info('Deleting Data Domain [' + name + ']. Options [' + JSON.stringify(request_options) + '].');
        return new Promise(function (resolve, reject) {
            var request = http.request(request_options, function (response) {
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    if (response_body_string && response_body_string.length) {
                        try {
                            var response_body_object = JSON.parse(response_body_string);
                            if (response_body_object.message.indexOf('not found') >= 0) {
                                console.info('Data Domain [' + name + '] does not exist.');
                                resolve(return_value_if_does_not_exist);
                            }
                            else {
                                var error_message = 'Error deleting Data Domain [' + name + ']. Message [' + response_body_object.message + '].';
                                console.error(error_message);
                                reject(new Error(error_message));
                            }
                        }
                        catch (e) {
                            var error_message = 'Error deleting Data Domain [' + name + ']. Response [' + response_body_string + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                    }
                    else {
                        console.info('Completed deleting Data Domain [' + name + '].');
                        resolve(true);
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error deleting Data Domain [' + name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.end();
        });
    };
    EDCClient.appendDataDomainsToDataObject = function (configuration, data_object_name, data_domain_names) {
        var document_id = configuration.resource_name + '://' + configuration.resource_path + '/' + data_object_name;
        var newFacts = [];
        var newSourceLinks = [];
        for (var i = 0; i < data_domain_names.length; i++) {
            var data_domain_name = data_domain_names[i];
            var newFact1 = {
                attrUuid: 'com.infa.ldm.profiling.DataDomainDataSetAll',
                value: 'DataDomain://' + data_domain_name
            };
            newFacts.push(newFact1);
            var newFact2 = {
                attrUuid: 'com.infa.ldm.profiling.DataDomainDataSetAccepted',
                value: 'DataDomain://' + data_domain_name
            };
            newFacts.push(newFact2);
            var newSourceLink1 = {
                associationId: 'com.infa.ldm.profiling.DataDomainDataSetAll',
                objectId: 'DataDomain://' + data_domain_name,
                properties: [
                    {
                        attrUuid: 'core.targetAttribute',
                        value: 'com.infa.ldm.profiling.DataDomainDataSetAll'
                    }
                ]
            };
            newSourceLinks.push(newSourceLink1);
            var newSourceLink2 = {
                associationId: 'com.infa.ldm.profiling.DataDomainTable',
                objectId: 'DataDomain://' + data_domain_name,
                properties: [
                    {
                        attrUuid: 'core.name',
                        value: document_id
                    }
                ]
            };
            newSourceLinks.push(newSourceLink2);
        }
        var request_body_object = {
            providerId: 'DataDomainScanner',
            updates: [
                {
                    id: document_id,
                    newFacts: newFacts,
                    newSourceLinks: newSourceLinks
                }
            ]
        };
        var request_body_string = JSON.stringify(request_body_object);
        console.log(request_body_string);
        var request_options = {
            method: 'PATCH',
            host: configuration.hostname,
            port: configuration.port,
            path: '/access/1/catalog/data/objects',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(request_body_string)
            }
        };
        request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration);
        console.info('Updating Data Object [' + data_object_name + '] with Data Domains. length [' + data_domain_names.length + ']. Options [' + JSON.stringify(request_options) + '].');
        return new Promise(function (resolve, reject) {
            var request = http.request(request_options, function (response) {
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    console.log(response_body_string);
                    try {
                        var response_body_object = JSON.parse(response_body_string);
                        if (response_body_object.message) {
                            var error_message = 'Error updating Data Object [' + data_object_name + '] with Data Domains. Response [' + response_body_object.message + '].';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else {
                            resolve(true);
                        }
                    }
                    catch (e) {
                        var error_message = 'Error updating Data Object [' + data_object_name + '] with Data Domains. Response [' + response_body_string + '].';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error updating Document (Object) [' + data_object_name + '] with Data Domains. length [' + data_domain_names.length + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.write(request_body_string);
            request.end();
        });
    };
    return EDCClient;
}());
exports.EDCClient = EDCClient;
//# sourceMappingURL=EDCClient.js.map